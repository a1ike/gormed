"use strict";

jQuery(function ($) {
  $(document).on("click", 'a[href^="#"]', function (event) {
    event.preventDefault();
    $("html, body").animate({
      scrollTop: $($.attr(this, "href")).offset().top
    }, 500);
  });
  $(".phone").inputmask({
    mask: "+7(999)-999-99-99",
    showMaskOnHover: false
  });
  $(".open-modal").on("click", function (e) {
    $(".g-modal").toggle();
  });
  $(".g-modal__centered").on("click", function (e) {
    if (e.target.className === "g-modal__centered") {
      $(".g-modal").hide();
    }
  });
  $(".g-modal__close").on("click", function (e) {
    $(".g-modal").hide();
  });
  $(".g-header__toggle").on("click", function (e) {
    e.preventDefault();
    $(this).toggleClass("g-header__toggle_active");
    $(".g-header__nav").slideToggle("fast");
    $(".g-header__phone").toggle();
    $(".g-header__button").toggle();
  });
  new Swiper(".g-doctors__cards", {
    navigation: {
      nextEl: ".g-doctors .swiper-button-next",
      prevEl: ".g-doctors .swiper-button-prev"
    },
    loop: true,
    spaceBetween: 30,
    slidesPerView: 1,
    breakpoints: {
      760: {
        slidesPerView: 2
      },
      1200: {
        slidesPerView: 3
      }
    }
  });
  new Swiper(".g-about__cards", {
    navigation: {
      nextEl: ".g-about__cards .swiper-button-next",
      prevEl: ".g-about__cards .swiper-button-prev"
    },
    loop: true,
    spaceBetween: 100
  });
  new Swiper(".g-reviews__cards", {
    navigation: {
      nextEl: ".g-reviews .swiper-button-next",
      prevEl: ".g-reviews .swiper-button-prev"
    },
    loop: true,
    spaceBetween: 170,
    slidesPerView: 1,
    breakpoints: {
      1200: {
        slidesPerView: 2
      }
    }
  });
});
//# sourceMappingURL=main.js.map